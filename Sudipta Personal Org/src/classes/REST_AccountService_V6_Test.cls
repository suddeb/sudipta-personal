@isTest
public class REST_AccountService_V6_Test {
    static testMethod void testDoPostWithCorrectData(){
        Account acc = setupTestData();
        REST_AccountService_V6.AccountWrapper accountWrapper = new REST_AccountService_V6.AccountWrapper();
        
        Test.startTest();
        // Set up the RestContext object
        System.RestContext.request = new RestRequest();
        System.RestContext.response = new RestResponse();
        RestContext.request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v6/accounts/';  
        RestContext.request.httpMethod = 'POST';
        accountWrapper = REST_AccountService_V6.doPost(acc);
        Test.stopTest();
        
        System.assertEquals(1, accountWrapper.accounts.size());
        System.assertEquals('Test', accountWrapper.accounts.get(0).Name);
        System.assertEquals('1232332', accountWrapper.accounts.get(0).AccountNumber);
        System.assertEquals('www.TEST.com', accountWrapper.accounts.get(0).Website);
        System.assertEquals('Success', accountWrapper.status);
        System.assertEquals('Inserted successfully', accountWrapper.message);
    }
    
    static testMethod void testDoPostWithIncorrectData(){
        Account acc = null;
        REST_AccountService_V6.AccountWrapper accountWrapper = new REST_AccountService_V6.AccountWrapper();
        
        Test.startTest();
        // Set up the RestContext object
        System.RestContext.request = new RestRequest();
        System.RestContext.response = new RestResponse();
        RestContext.request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v6/accounts/';  
        RestContext.request.httpMethod = 'POST';
        accountWrapper = REST_AccountService_V6.doPost(acc);
        Test.stopTest();
        
        System.assertEquals(null, accountWrapper.accounts);
        System.assertEquals('Failure', accountWrapper.status);
    }
    
    private static Account setupTestData(){
        Account acc=new Account();
	    acc.name='Test';
        acc.AccountNumber='1232332';
        acc.Website='www.TEST.com';
		return acc;    
	}
}