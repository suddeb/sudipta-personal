@isTest
public class REST_AccountService_V1_Test {
    static testMethod void testDoGetWithGoodId(){
        setupTestData();
        Account fetchedAccount = [Select ID from Account where Name='Account 1' LIMIT 1];
        Test.startTest();
        // Set up the RestContext object
        System.RestContext.request = new RestRequest();
        System.RestContext.response = new RestResponse();
        RestContext.request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v1/accounts/'+fetchedAccount.Id;  
        RestContext.request.httpMethod = 'GET';
        
        Account result = REST_AccountService_V1.doGet();
        Test.stopTest();
        
        System.assertEquals('Account 1', result.Name);
        System.assertEquals('(111) 222-3344', result.Phone);
        System.assertEquals('www.account1.com', result.Website);
    }
    
    private static void setupTestData() {
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name='Account 1', Phone='(111) 222-3344', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 2', Phone='(222) 333-4455', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 3', Phone='(333) 444-5566', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 4', Phone='(444) 555-6677', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 5', Phone='(555) 666-7788', Website='www.account1.com'));
        insert accounts;
    }
}