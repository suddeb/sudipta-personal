public class CountryController {
	private List<CountryDataWrapper> countryDataWrapper;
	
	public void parseJSONData(){
		countryDataWrapper = new ParseMultipleJsonData().parse();
		insertCountryInformation();
	}
	
	public List<CountryDataWrapper> getCountries(){
		return countryDataWrapper;
	}
	
	private void insertCountryInformation(){
		List<Country__c> allCountries = new List<Country__c>();
		for(CountryDataWrapper aCountryDataWrapper : countryDataWrapper){
			
			allCountries.add(new Country__c(
				Name = aCountryDataWrapper.countryName,
				Capital__c = aCountryDataWrapper.countryCapital,
				Currency__c = aCountryDataWrapper.countryCurrency
			));
		}
		Database.insert(allCountries);
	}
}