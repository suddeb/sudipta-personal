@isTest
public class REST_AccountService_V8_Test {

    static testMethod void testDoPostWithCorrectData(){
        REST_AccountService_V8.RequestWrapper requestWrapper = setupTestData();
        String jsonMessage = JSON.serialize(requestWrapper);
        REST_AccountService_V8.PostResponseWrapper postResponseWrapper = new REST_AccountService_V8.PostResponseWrapper();
        Test.startTest();
        // Set up the RestContext object
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v8/accounts/';  
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(jsonMessage);
        RestContext.request = request;
        RestContext.response = response;
        
        postResponseWrapper = REST_AccountService_V8.doPost(requestWrapper);
        Test.stopTest();
        
        System.assertEquals('Test', postResponseWrapper.account.Name);
        System.assertEquals('1232332', postResponseWrapper.account.AccountNumber);
        System.assertEquals('www.TEST.com', postResponseWrapper.account.Website);
        System.assertEquals(11, postResponseWrapper.contacts.size());
        System.assertEquals('Success', postResponseWrapper.status);
        System.assertEquals('1 Account and ' + postResponseWrapper.contacts.size() + ' contacts inserted successfully', postResponseWrapper.message);
    }
    
    static testMethod void testDoPostWithIncorrectData(){
        REST_AccountService_V8.RequestWrapper requestWrapper = null;
        String jsonMessage = JSON.serialize(requestWrapper);
        REST_AccountService_V8.PostResponseWrapper postResponseWrapper = new REST_AccountService_V8.PostResponseWrapper();
        Test.startTest();
        // Set up the RestContext object
        RestRequest request = new RestRequest();
        RestResponse response = new RestResponse();
        request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v8/accounts/';  
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(jsonMessage);
        RestContext.request = request;
        RestContext.response = response;
        
        postResponseWrapper = REST_AccountService_V8.doPost(requestWrapper);
        Test.stopTest();
        
        System.assertEquals(null, postResponseWrapper.account);
        System.assertEquals(null, postResponseWrapper.contacts);
        System.assertEquals('Failure', postResponseWrapper.status);
    }
    
    private static REST_AccountService_V8.RequestWrapper setupTestData(){
		Account acc=new Account();
	    acc.name='Test';
        acc.AccountNumber='1232332';
		acc.Website='www.TEST.com';

        List<contact> lstcon=new List<contact>();
	    for(Integer i=0;i<=10;i++){
        	Contact c=new Contact();
 		    c.lastname='Test+i';
		    lstcon.add(c);
	    }
        
        REST_AccountService_V8.RequestWrapper requestWrapper = new REST_AccountService_V8.RequestWrapper();
        requestWrapper.account = acc;
        requestWrapper.contacts = lstcon;
        
        return requestWrapper;
	}
}