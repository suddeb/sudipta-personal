public class CustomLogController {
	public void divideByZeroException(){
		Integer number1, number2;
		try{
			number1 = 10;
			number2 = 0;
			Integer number3 = number1/number2;	
		}catch(Exception ex){
			String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
			CustomLogging.logMessage('CustomLogController', 'divideByZeroException', message, CustomLogging.WARNING);
		}
	}
	
	public void nullPointerException(){
		Contact aContact = null;
		try{
			String email = aContact.Email;
		}catch(Exception ex){
			String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
			CustomLogging.logMessage('CustomLogController', 'nullPointerException', message, CustomLogging.ERROR);
		}
	}
}