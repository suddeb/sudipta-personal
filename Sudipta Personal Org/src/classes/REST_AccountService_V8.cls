/*
 * Example request
{
  "rqst" :
  {
    "account" : 
    { 
      "Name" : "Salesforce.com",
      "AccountNumber" : "1001",
      "Website" : "www.salesforce.com"
    },
    "contacts" :
    [
      {"FirstName" : "Test", "LastName" : "O'Riley", "Email" : "testcontact1@salesforce.com"},
      {"FirstName" : "Test", "LastName" : "Contact2", "Email" : "testcontact2@salesforce.com"},
      {"FirstName" : "Test", "LastName" : "Contact3", "Email" : "testcontact3@salesforce.com"}
    ]
  }
}
*/
@RestResource(urlMapping='/v8/accounts/*')
global with sharing class REST_AccountService_V8 {
	@HttpPost
    global static PostResponseWrapper doPost(RequestWrapper rqst){
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        PostResponseWrapper postResponseWrapper = new PostResponseWrapper();
        
        try{
            insert rqst.account;
            postResponseWrapper.account = rqst.account;
            
            for(Contact aContact : rqst.contacts){
                aContact.AccountId = rqst.account.Id;
            }
            insert rqst.contacts;
            postResponseWrapper.contacts = rqst.contacts;
            postResponseWrapper.status = 'Success';
            postResponseWrapper.message = '1 Account and ' + rqst.contacts.size() + ' contacts inserted successfully';
        }catch(Exception ex){
            postResponseWrapper.account = null;
            postResponseWrapper.contacts = null;
            postResponseWrapper.status = 'Failure';
            postResponseWrapper.message = ex.getMessage();
        }
        return postResponseWrapper;
    }
    
    global class RequestWrapper{
        public Account account;
        public List<Contact> contacts;
        public RequestWrapper(){}
    }
    
    global class PostResponseWrapper{
        public Account account;
        public List<Contact> contacts;
        public String status;
        public String message;
        public PostResponseWrapper(){}
    }
}