@RestResource(urlMapping='/v1/accounts/*')
global with sharing class REST_AccountService_V1 {
	
    @HttpGet
    global static Account doGet(){
		RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        
        String accountId = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        
        Account returnedAccount = [Select Id, Name, Phone, Website from Account Where Id = :accountId];
        return returnedAccount;
    }
}