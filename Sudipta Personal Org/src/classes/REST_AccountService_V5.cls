/*
 * Example request
{ 
   "name" : "ABCD",
   "phone" : "1111",
   "website": "www.google.com"
} 
*/
@RestResource(urlMapping='/v5/accounts/*')
global with sharing class REST_AccountService_V5 {
	@HttpPost
    global static AccountWrapper doPost(String name,String phone, String website){
    	RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        AccountWrapper accountWrapper = new AccountWrapper();
        
        Account account = new Account();
        account.Name = name;
        account.Phone = phone;
        account.Website = website;
        insert account;
        
        accountWrapper.accounts = new List<Account>();
        accountWrapper.accounts.add(account);
        accountWrapper.status = 'Success';
        accountWrapper.message = 'Account created succesfully';
        
        return accountWrapper;
    }
    
    global class AccountWrapper {
        public List<Account> accounts;
        public String status;
        public String message;
        
        public AccountWrapper(){}
    }
}