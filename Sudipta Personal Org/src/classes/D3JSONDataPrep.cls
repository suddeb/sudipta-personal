public class D3JSONDataPrep {
        public list<Airport__c> allAirports;
        
        public D3JSONDataPrep(){
                allAirports = [SELECT ID FROM Airport__c];
        }
        
        public String getAirportJson(){
                return JSON.serializePretty(allAirports);
        }
}