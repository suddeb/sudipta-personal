@isTest
private class TestMainWrapper {

    static testMethod void testJsonParser() {
        String jsonMessage = '{"studentId" : 1, "studentName" : "Sudipta Deb", "address" : "Kolkata, India"}';
        
        JSONParser parser = JSON.createParser(jsonMessage);
        MainWrapper m = (MainWrapper)parser.readValueAs(MainWrapper.class);
        
        System.assertEquals(1, m.studentId);
        System.assertEquals('Sudipta Deb', m.studentName);
        System.assertEquals('Kolkata, India', m.address);
    }
}