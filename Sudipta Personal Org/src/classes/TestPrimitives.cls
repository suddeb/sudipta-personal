public with sharing class TestPrimitives {
	public static void dateTest(){
		Date myDate = Date.newInstance(1984,6,14);
		Time myTime = Time.newInstance(18,2,56,4);
		System.debug(myDate);
		System.debug(myTime);
		
		Datetime myDateTime = Datetime.now();
		System.debug(myDateTime);
		
		Date today = Date.today();
		System.debug(today);
		
		Time t = Datetime.now().timeGmt();
		System.debug(t);
	}
	
	public static void listTest(){
		List<Integer> myList = new List<Integer>();
		System.debug('Initial size of list: ' + myList.size());
		
		myList.add(100);
		System.debug('Now the size of list: ' + myList.size());
		
		System.debug('First element of List: ' + myList.get(0));
		myList.set(0,200);
		System.debug('New First element of List: ' + myList.get(0));
	}
	
	public static void setTest(){
		Set<String> s = new Set<String>();
		s.add('Kolkata');
		s.add('Switzerland');
		
		System.debug('Size of set is: ' + s.size());
		if(s.contains('Switzerland')){
			System.debug('Switzerland is there');
		}
	}
}