public class AyncSummaryReportController {
	public List<SelectOption> availableReports { get; set; }
    public Id reportId { get; set; }
    public Id instanceId { get; set; }
    public Boolean reportIsRunning { get; set; }
    private transient Reports.ReportResults reportResults;
    
	public void checkMethod(){
		//Fetch all Reports
		List<Report> allReports = [Select Id, Name From Report Where Name = 'Opportunity Two Summary Report' Order By Name];
		System.Debug('ID: ' + allReports.get(0).Id);
		PageReference pageReference = runReport(allReports.get(0).Id);
		//validateResult();
	}
	
	public void clearAllLogs(){
		CustomLogging.cleanUpAllMessage();
	}
	
	private PageReference runReport(Id reportId) {
        Reports.ReportInstance reportInstance = Reports.ReportManager.runAsyncReport(reportId, true);
        instanceId = reportInstance.getId();
        processInstance(reportInstance);
 
        return null;
    }
    
    public void validateResult(){
    	checkForReportResults();
    	if(reportResults!=null){
    		
    		CustomLogging.logMessage('AyncSummaryReportController','validateResult', '*********** START FETCHING COLUMN HEADER ***********', CustomLogging.INFO);
    		fetchColumnHeader();
    		CustomLogging.logMessage('AyncSummaryReportController','validateResult', '*********** END FETCHING COLUMN HEADER ***********', CustomLogging.INFO);
    		
    		CustomLogging.logMessage('AyncSummaryReportController','validateResult', '*********** START SHOW ALL VALUES ***********', CustomLogging.INFO);
    		//showAllValues();
    		showAllValuesInTwoGrouping();
    		//understandDifference();
    		CustomLogging.logMessage('AyncSummaryReportController','validateResult', '*********** END SHOW ALL VALUES ***********', CustomLogging.INFO);
    	}
    }
    
    private void understandDifference(){
    	//Grouping Down is mainly used in summary report
    	// Get the down-grouping in the report
    	Reports.Dimension downGroupingDim = reportResults.getGroupingsDown();
    	//Get all row information
    	List<Reports.GroupingValue> allDownGroupings = downGroupingDim.getGroupings();
    	CustomLogging.logMessage('AyncSummaryReportController','understandDifference', 'Total Groupings: ' + allDownGroupings.size(), CustomLogging.INFO);
    	
    	//Grouping Across is mainly used in Matrix Report
    	// Get the down-grouping in the report
    	Reports.Dimension acrossGroupingDim = reportResults.getGroupingsAcross();
    	//Get all row information
    	List<Reports.GroupingValue> allAcrossGroupings = acrossGroupingDim.getGroupings();
    	CustomLogging.logMessage('AyncSummaryReportController','understandDifference', 'Total Groupings: ' + allAcrossGroupings.size(), CustomLogging.INFO);
    }
    
    private void showAllValuesInTwoGrouping(){
    	// Get the down-grouping in the report
    	Reports.Dimension dim = reportResults.getGroupingsDown();
    	
    	for(Reports.GroupingValue eachGrouping : dim.getGroupings()){
    		String stage = (String)eachGrouping.getValue();
    		
    		for(Reports.GroupingValue innerGrouping : eachGrouping.getGroupings()){
    			String fiscal_Year = (String)innerGrouping.getValue();
    			CustomLogging.logMessage('AyncSummaryReportController','showAllValuesInTwoGrouping', 'STAGE: ' + stage + 'FISCAL PERIOD: ' + fiscal_Year, CustomLogging.INFO);
    			
    			String factMapKey = innerGrouping.getKey()+'!T';
    			CustomLogging.logMessage('AyncSummaryReportController','showAllValuesInTwoGrouping', 'FactMap Key: ' + factMapKey, CustomLogging.INFO);
    			
    			// Get the fact map from the report results
				Reports.ReportFactWithDetails factDetails =
    				(Reports.ReportFactWithDetails)reportResults.getFactMap().get(factMapKey);
    				
    			//Get all Summary Values
    			//TODO : In one level of grouping, always get the first aggregate value
    			List<Reports.SummaryValue> allSumVals = factDetails.getAggregates();
    			for(Reports.SummaryValue eachSumValue : allSumVals){
    				CustomLogging.logMessage('AyncSummaryReportController','showAllValuesInTwoGrouping', 'Summary Value: ' + eachSumValue.getLabel(), CustomLogging.INFO);
    			}
    		
    			// Get the field value from the data cell of the row of the report
    			for(Reports.ReportDetailRow eachDetailRow : factDetails.getRows()){
    				for(Reports.ReportDataCell eachDataCell : eachDetailRow.getDataCells()){
    					CustomLogging.logMessage('AyncSummaryReportController','showAllValuesInTwoGrouping', 'Value: ' + eachDataCell.getLabel(), CustomLogging.INFO);
    				}
    			
    			}
    		}
    	}
    }
    
    /*
    * The below method will show all the values present in th report
    */
    private void showAllValues(){
    	// Get the down-grouping in the report
    	Reports.Dimension dim = reportResults.getGroupingsDown();
    	//Get all row information
    	List<Reports.GroupingValue> allGroupings = dim.getGroupings();
    	CustomLogging.logMessage('AyncSummaryReportController','showAllValues', 'Total Groupings: ' + allGroupings.size(), CustomLogging.INFO);
    	for(Reports.GroupingValue eachGrouping : allGroupings){
    		// Construct a fact map key, using the grouping key value
			String factMapKey = eachGrouping.getKey() + '!T';
			CustomLogging.logMessage('AyncSummaryReportController','showAllValues', 'FactMap Key: ' + factMapKey, CustomLogging.INFO);
			// Get the fact map from the report results
			Reports.ReportFactWithDetails factDetails =
    			(Reports.ReportFactWithDetails)reportResults.getFactMap().get(factMapKey);
    		
    		//Get all Summary Values
    		//TODO : In one level of grouping, always get the first aggregate value
    		List<Reports.SummaryValue> allSumVals = factDetails.getAggregates();
    		for(Reports.SummaryValue eachSumValue : allSumVals){
    			CustomLogging.logMessage('AyncSummaryReportController','showAllValues', 'Summary Value: ' + eachSumValue.getLabel(), CustomLogging.INFO);
    		}
    		
    		// Get the field value from the data cell of the row of the report
    		for(Reports.ReportDetailRow eachDetailRow : factDetails.getRows()){
    			for(Reports.ReportDataCell eachDataCell : eachDetailRow.getDataCells()){
    				CustomLogging.logMessage('AyncSummaryReportController','showAllValues', 'Value: ' + eachDataCell.getLabel(), CustomLogging.INFO);
    			}
    			
    		}
		}
    }
    
    /*
    * The below method will fetch the column header from the report
    */
    private void fetchColumnHeader(){
    	Map<String,Reports.DetailColumn> tempMap = reportResults.getReportExtendedMetadata().getDetailColumnInfo();
    	for(String key : tempMap.keySet() ){
    		String message = 'Name: ' + tempMap.get(key).getName() + ' Label: ' + tempMap.get(key).getLabel();
			CustomLogging.logMessage('AyncSummaryReportController','fetchExtendedMetaData', message, CustomLogging.INFO);
    	}
    }
    
    public PageReference checkForReportResults() {
        Reports.ReportInstance reportInstance = Reports.ReportManager.getReportInstance(instanceId);
        processInstance(reportInstance);
 
        return null;
    }
    
    private void processInstance(Reports.ReportInstance reportInstance) {
        reportIsRunning = reportInstance.getStatus() == 'Running' || reportInstance.getStatus() == 'New';
        if (!reportIsRunning) {
            reportResults = reportInstance.getReportResults();
        }
    }
}