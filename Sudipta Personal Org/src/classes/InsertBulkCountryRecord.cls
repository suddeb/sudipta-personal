public class InsertBulkCountryRecord {
	public static void insertData(){
		
		
		List<Country__c> allCountries = new List<Country__c>();
		for(Integer i=0;i<9999;i++){
			Country__c newCountry = new Country__c(
				Name='India',
				Capital__c='Delhi',
				Currency__c='Rupee'
			);
			allCountries.add(newCountry);
		}
		
		Database.insert(allCountries);
	}
}