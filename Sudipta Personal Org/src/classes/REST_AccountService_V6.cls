/*
 * Example request
{ "account" :
 {
   "Name" : "ABCD",
   "Phone" : "1111",
   "Website": "www.google.com"
 }
} 
*/
@RestResource(urlMapping='/v6/accounts/*')
global with sharing class REST_AccountService_V6 {
    @HttpPost
    global static AccountWrapper doPost(Account account){
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        AccountWrapper accountWrapper = new AccountWrapper();
        
        try{
            insert account;
            accountWrapper.accounts = new List<Account>();
            accountWrapper.accounts.add(account);
            accountWrapper.status = 'Success';
            accountWrapper.message = 'Inserted successfully';
        }catch(Exception ex){
            accountWrapper.accounts = null;
            accountWrapper.status = 'Failure';
            accountWrapper.message = ex.getMessage();
            response.statusCode = 404;
        }
        
        return accountWrapper;
        
    }
    global class AccountWrapper {
        public List<Account> accounts;
        public String status;
        public String message;
        
        public AccountWrapper(){}
    }
}