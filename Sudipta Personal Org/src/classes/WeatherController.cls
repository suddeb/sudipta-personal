public class WeatherController {
	public WeatherWrapper myWeatherWrapper;
	public Double temp_f;
	public Double temp_c;
	public String pressure;
	
	public void fetchWeatherData(){
		HttpRequest req = new HttpRequest();
		req.setEndpoint('http://api.wunderground.com/api/d05c241b0fce06eb/conditions/q/CA/San_Francisco.json');
  		req.setMethod('GET');
  		Http http = new Http();
  		HttpResponse res = new HttpResponse();
  		res = http.send(req);
  		this.parseWeatherData(res.getBody());
	}
	
	public Double getTemp_f(){
		if(null == myWeatherWrapper){
			return 0;
		}else {
			return myWeatherWrapper.Current_observation.temp_f;
		}
	}
	
	public Double getTemp_c(){
		if(null == myWeatherWrapper){
			return 0;
		}else {
			return myWeatherWrapper.Current_observation.temp_c;
		}
	}
	
	public String getPressure(){
		if(null == myWeatherWrapper){
			return '';
		}else {
			return myWeatherWrapper.Current_observation.pressure_mb;
		}
	}
	
	private void parseWeatherData(String jsonString){
		myWeatherWrapper = (WeatherWrapper) System.JSON.deserialize(jsonString, WeatherWrapper.class);
	}
}