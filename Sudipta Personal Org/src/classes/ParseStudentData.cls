public class ParseStudentData {
	private StudentDetailWrapper m;
	
	public StudentDetailWrapper parseData(String jsonMessage){
		return doParsing(jsonMessage);
	}
	
	public StudentDetailWrapper parseData(){
		String jsonMessage = '{"firstName":"Sudipta","lastName":"Deb","PlacesVisited":{"HillStation":["Kashmir","Darjeeling","Zermatt","Murren"],"SeaSide":["Goa","Santorini"],"Desert":["Sahara","Rajasthan"]},"language":["German","English","Bengali"]}';
		return doParsing(jsonMessage);
	}
	
	private StudentDetailWrapper doParsing(String jsonMessage){
		CustomLogging.logMessage('StudentDetailController', 'parseJSONData', 'Starting of parseJSONData()', CustomLogging.INFO);
		try{
			m = (StudentDetailWrapper) System.JSON.deserialize(jsonMessage, StudentDetailWrapper.class);
		}catch(Exception ex){
			String message = 'Error: ' + ex.getMessage() + '; Stack Trace:' + ex.getStackTraceString();
			CustomLogging.logMessage('ParseStudentData', 'parseJSONData', message, CustomLogging.ERROR);
		}
		return m;
	}
}