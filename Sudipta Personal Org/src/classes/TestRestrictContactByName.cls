@isTest
public class TestRestrictContactByName {
	@isTest
	static void testRestrictContactByName(){
		Contact aContact = new Contact(FirstName='VALID NAME',LastName='INVALIDNAME');
		
		//Perform Test
		Test.startTest();
		Database.SaveResult result = Database.insert(aContact,false);
		Test.stopTest();
		
		System.assert(!result.isSuccess());
		System.assert(result.getErrors().size() > 0);
		System.assertEquals('The Last Name "INVALIDNAME" is not allowed for DML',result.getErrors()[0].getMessage());
		
	}
}