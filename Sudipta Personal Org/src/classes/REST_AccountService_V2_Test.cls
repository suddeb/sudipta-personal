@isTest
public class REST_AccountService_V2_Test {
    static testMethod void testdoGetWithCorrectId(){
        setupTestData();
        Account fetchedAccount = [Select ID from Account where Name='Account 1' LIMIT 1];
        Test.startTest();
        // Set up the RestContext object
        System.RestContext.request = new RestRequest();
        System.RestContext.response = new RestResponse();
        RestContext.request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v1/accounts/'+fetchedAccount.Id;  
        RestContext.request.httpMethod = 'GET';
        
        REST_AccountService_V2.AccountWrapper accountWrapper = new REST_AccountService_V2.AccountWrapper();
        accountWrapper = REST_AccountService_V2.doGet();
        Test.stopTest();
        
        System.assertEquals('Account 1', accountWrapper.account.Name);
        System.assertEquals('(111) 222-3344', accountWrapper.account.Phone);
        System.assertEquals('www.account1.com', accountWrapper.account.Website);
        System.assertEquals('Success', accountWrapper.status);
        System.assertEquals('Found the account', accountWrapper.message);
    }
    
    static testMethod void testdoGetWithWrongId(){
        Test.startTest();
        // Set up the RestContext object
        System.RestContext.request = new RestRequest();
        System.RestContext.response = new RestResponse();
        RestContext.request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v1/accounts/111';  
        RestContext.request.httpMethod = 'GET';
        
        REST_AccountService_V2.AccountWrapper accountWrapper = new REST_AccountService_V2.AccountWrapper();
        accountWrapper = REST_AccountService_V2.doGet();
        Test.stopTest();
        
        System.assertEquals(null, accountWrapper.account);
        System.assertEquals('Failure', accountWrapper.status);
        System.assertEquals('This account could not be found', accountWrapper.message);
    }
    
    private static void setupTestData() {
        List<Account> accounts = new List<Account>();
        accounts.add(new Account(Name='Account 1', Phone='(111) 222-3344', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 2', Phone='(222) 333-4455', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 3', Phone='(333) 444-5566', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 4', Phone='(444) 555-6677', Website='www.account1.com'));
        accounts.add(new Account(Name='Account 5', Phone='(555) 666-7788', Website='www.account1.com'));
        insert accounts;
    }
}