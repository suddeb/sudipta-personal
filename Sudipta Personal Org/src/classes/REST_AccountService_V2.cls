@RestResource(urlMapping='/v2/accounts/*')
global with sharing class REST_AccountService_V2 {
	@HttpGet
    global static AccountWrapper doGet(){
    	RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        AccountWrapper accountWrapper = new AccountWrapper();
        
        String accountId = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        
        List<Account> fetchedAccounts = [Select Id, Name, Phone, Website from Account Where Id = :accountId];
        if(fetchedAccounts != null && fetchedAccounts.size() > 0){
            accountWrapper.account = fetchedAccounts.get(0);
            accountWrapper.status = 'Success';
            accountWrapper.message = 'Found the account';
        }else{
            accountWrapper.account = null;
            accountWrapper.status = 'Failure';
            accountWrapper.message = 'This account could not be found';
            response.statusCode = 404;
        }
        return accountWrapper;
    }
    
    global class AccountWrapper {
        public Account account;
        public String status;
        public String message;
        
        public AccountWrapper(){}
    }
}