public class SalesforceLoginController {
        public String selectedOrg{get; set;}
        public String userName{get;set;}
        public String password{get;set;}
        
        
        public List<SelectOption> getAvailableOrgs(){
                List<SelectOption> options = new List<SelectOption>();
                options.add(new SelectOption('login','PRODUCTION'));
                options.add(new SelectOption('test','SANDBOX'));
                return options;
        }
        
        
    /**
    * @description : This method does the login to sf orgs and provides session id and 
    *  instance url wrapped in SFOrgLogin wrapper instances.
    */
    @RemoteAction
    public static List<SFOrgLogin> doSFlogin(String orgType, String userName, String password){
        List<SFOrgLogin> sfOrgLogins = new List<SFOrgLogin>();
        sforgLogins.add(getSessionId(userName,password,orgType));
        //System.assert(false,'Sudipta');
    	return sforgLogins;
    }
        
                /**
         * @description : This gets the sesssion id and instance url from any SF org based on 
         * username, password and orgType(Production/Sandbox)
         */
        public static SFOrgLogin getSessionId(String username, String password,String orgType){
            HttpRequest req = new HttpRequest();
            HttpResponse res = new HttpResponse();
            Http http = new Http();
            
            req.setEndpoint('https://'+orgType+'.salesforce.com/services/oauth2/token');
            req.setMethod('POST');
            req.setBody('grant_type=password&client_id=sudipta.deb.2014@gmail.com&client_secret=UFWHt98MAJMKhKEc34NGOL7C&username='+EncodingUtil.urlEncode(username, 'UTF-8')+'&password='+EncodingUtil.urlEncode(password, 'UTF-8'));
            req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            
            try {
                res = http.send(req);
                System.debug(res.getBody());
                Map<String,Object> response = (Map<String,Object>)JSON.deserializeUntyped(res.getBody());
                SFOrgLogin sforg = new SFOrgLogin();
                sforg.sessionId = ''+response.get('access_token');
                sforg.instanceURL = ''+response.get('instance_url');
                if(sforg.sessionId == '' || sforg.sessionId == 'null'
                    || sforg.instanceURL == '' || sforg.instanceURL == 'null'){
                    System.assert(false, 'SUDIPTA');
                    throw new SFCompareException('Username/Password is incorrect - Please check details for '+username);
                }else{
                        System.Debug('SUDIPTA: Login Successful');
                }
                return sforg;
            } catch(System.CalloutException e) {
                System.debug('Calloutor: '+ e);
                System.debug(res.toString());
            }
            
            return null;
        }
        /**
         * @ApexClass : SFOrgLogin
         * @Description : Wrapper to hold session id and instance url.
         */
        public class SFOrgLogin{
            String sessionId;
            String instanceURL;
        }
        
        /**
        * @ApexClass    : SFCompareException 
        * @Description  : Custom Exception 
        */
       public class SFCompareException extends Exception {}
}