public class TestEmail {
    private final String emailAddress = 'sudipta.deb@gmail.com';
    private final Contact con;
    public TestEmail(){
        
    }
    
    public void sendEmail(){
        try{
            Contact aContact = [Select id, Email from Contact where Email = 'test@organisation.com'];
            
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
            List<String> sendTo = new List<String>();
            sendTo.add(emailAddress);
            
            //get recipient size
            Integer recipientSize = 0;
            if(sendTo <> null){
                recipientSize += sendTo.size();
            }
            System.debug('Now Recipient Size: ' + recipientSize);
            
            Messaging.reserveSingleEmailCapacity(Integer.valueOf(recipientSize));
            System.debug('Send email limit passed');
            
            mail.setToAddresses(sendTo);
            mail.setTargetObjectId(aContact.id);
            mail.setReplyTo(aContact.Email);
            mail.setSenderDisplayName('Sudipta Deb - Cognizant');
        
            mail.setTemplateId('00X90000001BDiE');
        
            mails.add(mail);
        
            Messaging.sendEmail(mails);
            mails = null;  
            sendTo = null;      
        }catch(Exception e){
            System.debug('Sending Email error :'+e);            
        }  
        
    }
}