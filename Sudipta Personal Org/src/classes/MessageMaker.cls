public class MessageMaker{

    public Integer myInt {
        get {return myInt;}   
        set {myInt = value; if(value < 0) myInt = 0;}
    }
    public static String helloMessage(){
        return 'You say "GoodBye", I say "Hello"';
    }
}