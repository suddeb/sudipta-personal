public class StudentDetailWrapper {
	
	public String firstName;
	public List<String> language;
	public String lastName;
	public PlacesVisited PlacesVisited;
	
	public class PlacesVisited {
		public List<String> HillStation;
		public List<String> SeaSide;
		public List<String> Desert;
	}
}