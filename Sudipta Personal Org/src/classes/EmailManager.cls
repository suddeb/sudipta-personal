public class EmailManager {
	public void sendEmail(String address, String subject, String body){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[]{address};
		mail.setToAddresses(toAddresses);
		mail.setSubject(subject);
		mail.setPlainTextBody(body);
		
		Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
		
		inspectResult(results);
	}
	
	private Boolean inspectResult(Messaging.SendEmailResult[] results){
		Boolean sendResult = true;
		for(Messaging.SendEmailResult eachResult : results){
			if(eachResult.isSuccess()){
				System.Debug('Email send successfully');
			}else{
				sendResult = false;
				System.Debug('The following error occurred: ' + eachResult.getErrors());
			}
		}
		return sendResult;
	}
}