public class ContactAndLeadSearch {
	public static List<List< SObject>> searchContactsAndLeads(String searchValue){
		List<List< SObject>> returnValues = [FIND :searchValue IN NAME Fields
						RETURNING Contact(Name), Lead(Name)];
		return returnValues;
	}
}