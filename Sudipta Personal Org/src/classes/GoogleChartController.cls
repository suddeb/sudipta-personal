public with sharing class GoogleChartController {
	
	@RemoteAction
    public static List<Bubble> getUpdatedData() {
    	//CustomLogging.logMessage('GoogleChartController','getUpdatedData', '*********** INSIDE ***********', CustomLogging.INFO);
        List<Bubble> bubbles = createBubbles();
        return bubbles;
    }
    
    private static List<Bubble> createBubbles(){
    	Bubble bubble1 = new Bubble('Needs Analysis',10234,4);
    	Bubble bubble2 = new Bubble('Proposal/Price Quote',200234,2);
    	Bubble bubble3 = new Bubble('Negotiation/Review',400234,3);
    	Bubble bubble4 = new Bubble('Closed Won',300234,2);
    	
    	List<Bubble> tempList = new List<Bubble>();
    	tempList.add(bubble1);
    	tempList.add(bubble2);
    	tempList.add(bubble3);
    	tempList.add(bubble4);
    	
    	return tempList;
    }
    
    class Bubble {
        public String stage { get; set; }
        public Decimal amount { get; set; }
        public Integer rowCount { get; set; }
 
        public Bubble(String s, Decimal a, Integer rc) {
            stage = s;
            amount = a;
            rowCount = rc;
        }
    }
}