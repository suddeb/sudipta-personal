public class DeleteRecordsController {
	public void deleteAllCountryRecords(){
		List<Country__c> existing = [select Id from Country__c];
		delete existing;
	}
	
	public void deleteAllCustomLogRecords(){
		List<Custom_Log__c> existing = [select Id from Custom_Log__c];
		delete existing;
	}
	
	public void deleteAllAirportRecords(){
		List<Airport__c> existing = [select Id from Airport__c];
		delete existing;
	}
}