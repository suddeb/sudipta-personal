@isTest
private class TestStudentDetailController {
	
	private static void setEnableCustomLoggingFlag(Boolean value){
		insert(new Decision_Object__c(Name='EnableCustomLogging', Value__c=value));
	}
	
	static testMethod void testJsonParseData() {
		setEnableCustomLoggingFlag(false);
		String jsonMessage = '{"firstName" : "Paromita", "lastName" : "Banerjee","language": ["German", "English", "Bengali"]}';
    
        StudentDetailController studentDetailController = new StudentDetailController();
        studentDetailController.parseJSONData(jsonMessage);
        
        System.assertEquals('Paromita',studentDetailController.firstName);
        System.assertEquals('Banerjee',studentDetailController.lastName);
        System.assertEquals(3,studentDetailController.getLanguageOptions().size());
    }
}