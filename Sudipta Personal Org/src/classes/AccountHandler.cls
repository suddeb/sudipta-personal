public with sharing class AccountHandler {
	public static Account insertNewAccount(String nameOfAccount){
		Account returnAccount = null;
		Account anAccount = new Account();
		if(!nameOfAccount.equals('')){
			anAccount.Name = nameOfAccount;
		}
		
		try{
			insert anAccount;
			
			returnAccount = [SELECT ID, Name FROM ACCOUNT WHERE Name = :nameOfAccount];
		}catch(DmlException e){
			System.Debug('DML Exception occurred: ' + e.getMessage());
		}
		
		return returnAccount;
	}
}