/*
 * Example request
{ "accounts" :
  [{
    "Name" : "A1",
    "Phone" : "1111",
    "Website": "www.google.com"
  },
  {
    "Name" : "A2",
    "Phone" : "1111"
  }]
}
*/
@RestResource(urlMapping='/v7/accounts/*')
global with sharing class REST_AccountService_V7 {
	@HttpPost
    global static AccountWrapper doPost(List<Account> accounts){
        RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        AccountWrapper accountWrapper = new AccountWrapper();
        
        try{
            insert accounts;
            accountWrapper.accounts = accounts;
            accountWrapper.status = 'Success';
            accountWrapper.message = accounts.size() + ' accounts inserted successfully';
        }catch(Exception ex){
            accountWrapper.accounts = null;
            accountWrapper.status = 'Failure';
            accountWrapper.message = ex.getMessage();
            response.statusCode = 404;
        }
        
        return accountWrapper;
    }
    global class AccountWrapper {
        public List<Account> accounts;
        public String status;
        public String message;
        
        public AccountWrapper(){}
    }
}