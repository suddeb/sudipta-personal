@isTest
public class TestVerifyDate {
	@isTest
	static void testTwoDateWithin30Days(){
		Date returnedDate = VerifyDate.CheckDates(Date.newInstance(2015, 3, 1), Date.newInstance(2015, 3, 22));
		System.assertEquals(Date.newInstance(2015, 3, 22), returnedDate);
	}
	
	@isTest
	static void testEndOfMonthDate(){
		Date returnedDate = VerifyDate.CheckDates(Date.newInstance(2015, 3, 1), Date.newInstance(2015, 8, 22));
		System.assertEquals(Date.newInstance(2015, 3, 31), returnedDate);
	}
}