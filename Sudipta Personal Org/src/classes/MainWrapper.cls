public class MainWrapper {
	public Integer studentId;
	public String studentName;
	public String address;
	
	public MainWrapper(Integer studentId, String studentName, String address) {
		this.studentId = studentId;
		this.studentName = studentName;
		this.address = address;
	}
}