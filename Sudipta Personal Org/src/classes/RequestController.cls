public class RequestController {
	public String request1{get; set;}
	public String request2{get; set;}
	public String selectedOperation{get; set;}
	
	
	public void generateRequest1(){
		System.Debug('Inside createRequest() method');
		DOM.Document body = new DOM.Document();
		String prefix = 'Sudipta';
		String nameSpace = selectedOperation;
		String method = 'createResponse';
		
		DOM.XmlNode root = body.createRootElement('topRequestTag', nameSpace, prefix);
		
		List<String> allRequieredFields = fetchAccountFields();
		String requiredFields = String.join(allRequieredFields, ',');
		
		List<Account> allAccounts = Database.query('SELECT ' + requiredFields + ', (select id, name, email from Contacts) FROM ACCOUNT');
		
		//Add child element
		for(Account anAccount : allAccounts){
			Dom.XmlNode accountNode = root.addChildElement('Account', null, null);
			accountNode.setAttribute('ID', anAccount.ID);
			accountNode.setAttribute('NAME', anAccount.Name);
			
			for (Contact eachContact : anAccount.Contacts) {
            	Dom.Xmlnode contactNode = accountNode.addChildElement('Contact', null, null);
            	contactNode.setAttribute('id', eachContact.Id);
            	contactNode.setAttribute('name', eachContact.Name);
            	if(eachContact.Email!=null ){
            		contactNode.setAttribute('email', eachContact.Email);
            	}
			}
        }
		
		request1 = body.toXmlString();
	}
	
	public void generateRequest2(){
		DOM.Document body = new DOM.Document();
		String prefix = 'SD';
		String nameSpace = selectedOperation;
		String method = 'createAccountRequest';
		
		DOM.XmlNode root = body.createRootElement('topResponseTag', nameSpace, prefix);
		
		List<SD_Service_Fields__c> serviceFields = [
				SELECT Name, Request_Name__c, Sequence__c, Namespace_Prefix__c, Parent_Node__c, Type__c, XmlLabel__c, Source_Object__c, Field_Name__c
				FROM SD_Service_Fields__c 
				WHERE Request_Name__c = :method
				ORDER BY Sequence__c];
		System.Debug('Found: ' + serviceFields.size());
		
		List<String> allRequieredFields = fetchAccountFields();
		String requiredFields = String.join(allRequieredFields, ',');
		
		List<Account> allAccounts = Database.query('SELECT ' + requiredFields + ' FROM ACCOUNT');
		
		//Add Account Details one by one
		for(Account eachAccount : allAccounts){
			Dom.XmlNode accountNode = root.addChildElement(method, nameSpace, prefix);
			appendSObjectToXml(accountNode, eachAccount, serviceFields);
		}
		
		request2 = body.toXmlString();
	}
	
	private List<String> fetchAccountFields(){
		List<SD_Service_Fields__c> allServiceFields = [
				SELECT Name, Field_Name__c FROM SD_Service_Fields__c 
				WHERE Type__c = 'Field' and Source_Object__c = 'Account'];
		List<String> allFields = new List<String>();
		for(SD_Service_Fields__c aServiceField : allServiceFields){
			allFields.add(aServiceField.Field_Name__c);
		}
		return allFields;
	}
	
	private void appendSObjectToXml(Dom.XmlNode accountNode, Account eachAccount, List<SD_Service_Fields__c> serviceFields){
		Map<string, dom.xmlnode> xmlnodemap = new Map<string, dom.xmlnode>();
		//get Namespaces from custom setting
		Map<String,SD_Namespaces__c> allNameSpaces = SD_Namespaces__c.getall();
			
		for(SD_Service_Fields__c field : serviceFields){
			if(field.Type__c == 'XmlNode'){
				if(field.Parent_Node__c == 'root'){
					xmlnodemap.put(field.XmlLabel__c, accountNode);
				}else{
					xmlnodemap.put(field.XmlLabel__c, 
									xmlnodemap.get(field.parent_Node__c).addChildElement(field.XmlLabel__c, 
																						allNameSpaces.get(field.Namespace_Prefix__c).namespace__c, 
																						field.Namespace_Prefix__c));
				}
			}else if(field.Type__c == 'Field'){
				String innerText = '';
				Boolean isNull = true;
				
				//get the required field 
				String fieldRequired = field.Field_Name__c;
				sObject anAccount = eachAccount;
				Object value = anAccount.get(fieldRequired);
				System.Debug('Field: ' + fieldRequired + ' Value: ' + String.valueOf(value));
				
				if(value != null){
					innerText = String.valueOf(value);
					isNull = false;
				}
				
				if(!isNull){
					DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xmlLabel__c, 
																								allNameSpaces.get(field.Namespace_Prefix__c).namespace__c, 
																								field.Namespace_Prefix__c);	            				
				    xmlField.addTextNode(innerText); 
				}
			}else if(field.Type__c == 'Constant'){
				
			}
		}
	}
	
	public List<SelectOption> getPossibleOperations(){
		List<SelectOption> possibleOperations = new List<SelectOption>();
		
		//Fetch all namespaces available from Custom Settings
		Map<String,SD_Namespaces__c> allNameSpaces = SD_Namespaces__c.getall();
		
		//Create the select option
		for(String operationName: allNameSpaces.keySet()){
			possibleOperations.add(new SelectOption(allNameSpaces.get(operationName).Namespace__c, 
										allNameSpaces.get(operationName).Name));
		}
		
		return possibleOperations;
	}
}