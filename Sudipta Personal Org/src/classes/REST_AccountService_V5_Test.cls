@isTest
public class REST_AccountService_V5_Test {
    static testMethod void testDoPost(){
		REST_AccountService_V5.AccountWrapper accountWrapper = new REST_AccountService_V5.AccountWrapper();
        Test.startTest();
        // Set up the RestContext object
        System.RestContext.request = new RestRequest();
        System.RestContext.response = new RestResponse();
        RestContext.request.requestURI = 'https://ap1.salesforce.com/services/apexrest/v5/accounts/';  
        RestContext.request.httpMethod = 'POST';
        accountWrapper = REST_AccountService_V5.doPost('XXXX','11111','www.test.com');
        Test.stopTest();
        
        System.assertEquals(1, accountWrapper.accounts.size());
        System.assertEquals('XXXX', accountWrapper.accounts.get(0).Name);
        System.assertEquals('11111', accountWrapper.accounts.get(0).Phone);
        System.assertEquals('www.test.com', accountWrapper.accounts.get(0).Website);
        System.assertEquals('Success', accountWrapper.status);
        System.assertEquals('Account created succesfully', accountWrapper.message);
    }
}