public class CustomLogging {
	
	public static String INFO = 'INFO';
	public static String DEBUG = 'DEBUG';
	public static String WARNING = 'WARNING';
	public static String ERROR = 'ERROR';
	public static String FATAL = 'FATAL';
	
	private static Boolean isEnabled(){
		Decision_Object__c enableCustomLogging = Decision_Object__c.getInstance('EnableCustomLogging');
		System.debug('IsEnabled: ' + enableCustomLogging.Value__c);
		return enableCustomLogging.Value__c;
	}
	
	public static void logMessage(String className, String methodName, String message, String Priority){
		if(isEnabled()){
			Custom_Log__c newLogMessage = new Custom_Log__c(
				Class__c = className,
				Method__c = methodName,
				Message__c = message,
				Priority__c = Priority);
			try{
				Database.insert(newLogMessage);
			}catch(Exception ex){
				System.debug(
					'Failed to INSERT the [Apex Debug Log] ADL record. ' +
                	'Error: ' + ex.getMessage()
				);
			}
		}else{
			String completeErrorMessage = 'Error occured at class: ' + className + ' method: ' + methodName + 
				' and the error is: ' + message;
			System.debug(completeErrorMessage);
		}
		
	}
	
	public static void cleanUpAllMessage(){
		List<Custom_Log__c> allCustomLogs = [SELECT ID from Custom_Log__c];
		Database.delete(allCustomLogs);
	}
}