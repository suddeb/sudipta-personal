public class StudentDetailController{  
	public String firstName{get;set;}
	public String lastName{get;set;}
	public String selectedLanguage{get;set;}
	public String selectedHillStation{get;set;}
	public String selectedSeaSide{get;set;}
	public String selectedDesert{get;set;}
	private StudentDetailWrapper m;
	public List<SelectOption> languageOptions;
	public List<SelectOption> hillStations;
	public List<SelectOption> seaSides;
	public List<SelectOption> deserts;
	
	public void parseJSONData(){
    	m = new ParseStudentData().parseData();
    	doAllInitialization();
    }
    
    public void parseJSONData(String jsonMessage){
    	m = new ParseStudentData().parseData(jsonMessage);
    	doAllInitialization();
    }
    
    public List<SelectOption> getLanguageOptions(){
    	if(null == languageOptions){
    		languageOptions = new List<SelectOption>();
    	}
    	return languageOptions;
    }
    
    public List<SelectOption> getHillStations(){
    	if(null == hillStations){
    		hillStations = new List<SelectOption>();
    	}
    	return hillStations;
    }
    
    public List<SelectOption> getSeaSides(){
    	if(null == seaSides){
    		seaSides = new List<SelectOption>();
    	}
    	return seaSides;
    }
    
    public List<SelectOption> getDeserts(){
    	if(null == deserts){
    		deserts = new List<SelectOption>();
    	}
    	return deserts;
    } 
    
    private void doAllInitialization(){
    	initializeStudentName();
    	initializeLanguageOptions(m.language);
    	initializeHillStations(m.PlacesVisited.HillStation);
    	initializeSeaSides(m.PlacesVisited.SeaSide);
    	initializeDeserts(m.PlacesVisited.Desert);
    }
    
    private void initializeStudentName(){
    	this.firstName = m.firstName;
    	this.lastName = m.lastName;
    }
    
    private void initializeLanguageOptions(List<String> language){
    	languageOptions = new List<SelectOption>();
    	for(String aLanguage : language){
    		languageOptions.add(new SelectOption(aLanguage, aLanguage));
    	}
    }
    
    private void initializeHillStations(List<String> allHillStations){
    	hillStations = new List<SelectOption>();
    	for(String aHillStations : allHillStations){
    		hillStations.add(new SelectOption(aHillStations, aHillStations));
    	}
    }
    
    private void initializeSeaSides(List<String> allSeaSides){
    	seaSides = new List<SelectOption>();
    	for(String aSeaSide : allSeaSides){
    		seaSides.add(new SelectOption(aSeaSide, aSeaSide));
    	}
    }
    
    private void initializeDeserts(List<String> allDeserts){
    	deserts = new List<SelectOption>();
    	for(String aDeserts : allDeserts){
    		deserts.add(new SelectOption(aDeserts, aDeserts));
    	}
    }
}