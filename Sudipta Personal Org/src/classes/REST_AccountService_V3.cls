@RestResource(urlMapping='/v3/accounts/*')
global with sharing class REST_AccountService_V3 {
	@HttpGet
    global static AccountWrapper doGet(){
    	RestRequest request = RestContext.request;
        RestResponse response = RestContext.response;
        AccountWrapper accountWrapper = new AccountWrapper();
        
        String entireString = request.requestURI;
        Integer index = request.requestURI.lastIndexOf('/');
        String accountId = request.requestURI.substring(request.requestURI.lastIndexOf('/')+1);
        
        System.debug('Entire String: ' +  entireString);
        System.debug('First String: ' +  index);
        System.debug('Account ID: ' +  accountId);
        if(doSearch(accountId)){
            searchAccounts(request,response,accountWrapper);
        }else{
        	findAccount(request,response,accountId, accountWrapper);
        }
        
        return accountWrapper;
    }
    
    private static Boolean doSearch(String accountId){
        System.debug('AccountID came: ' + accountId);
        
        if(accountId.equals('accounts')){
            return true;
        }else{
            return false;
        }
    }
    
    private static void searchAccounts(RestRequest request, RestResponse response, AccountWrapper accountWrapper){
        //Use RestRequest's params to fetch the name parameter
        String searchTerm = request.params.get('name');
        
        if(searchTerm != null && searchTerm.equals('')){
            accountWrapper.accounts = null;
            accountWrapper.status = 'Error';
            accountWrapper.message = 'You must provide a name';
        }else{
            String searchText = '%'+searchTerm+'%';
            List<Account> fetchedAccounts = [Select Id, Name, AccountNumber from Account Where Name Like :searchText];
            
            if(fetchedAccounts != null && fetchedAccounts.size() > 0){
                accountWrapper.accounts = fetchedAccounts;
                accountWrapper.status = 'Success';
            	accountWrapper.message = fetchedAccounts.size() + 'accounts found with the search result';
            }else{
                accountWrapper.accounts = null;
                accountWrapper.status = 'Failure';
            	accountWrapper.message = 'No account found';
                response.statusCode = 404;
            }
        }
    }
    
    private static void findAccount(RestRequest request, RestResponse response,String accountId, AccountWrapper accountWrapper ){
        List<Account> fetchedAccounts = [Select Id, Name, AccountNumber from Account Where Id = :accountId];
        
        if(fetchedAccounts != null && fetchedAccounts.size() > 0){
            accountWrapper.accounts = new List<Account>();
            accountWrapper.accounts.add(fetchedAccounts.get(0));
            accountWrapper.status = 'Success';
            accountWrapper.message = 'Account Found';
        }else{
			accountWrapper.accounts = null;
            accountWrapper.status = 'Failure';
            accountWrapper.message = 'No Account found';
			response.statusCode = 404;            
        }
    }
    
    global class AccountWrapper {
        public List<Account> accounts;
        public String status;
        public String message;
        
        public AccountWrapper(){}
    }
}