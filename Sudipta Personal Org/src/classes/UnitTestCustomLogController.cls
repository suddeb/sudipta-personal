@isTest
private class UnitTestCustomLogController {
	
	private static void setEnableCustomLoggingFlag(Boolean value){
		insert(new Decision_Object__c(Name='EnableCustomLogging', Value__c=value));
	}
    
    static testMethod void testWhetherCustomLoggingIsGettingCreatedWhileDisabled() {
    	setEnableCustomLoggingFlag(false);
        CustomLogController customLogController = new CustomLogController();
    	customLogController.divideByZeroException();
        
        Integer numberOfCustomLogs= [Select count() From Custom_Log__c];
        System.assertEquals(0, numberOfCustomLogs);
    }
    
    static testMethod void testDivideByZeroException(){
    	setEnableCustomLoggingFlag(true);
    	CustomLogController customLogController = new CustomLogController();
    	customLogController.divideByZeroException();
    	
    	Integer numberOfCustomLogs= [Select count() From Custom_Log__c];
    	Custom_Log__c customLog= [Select Class__c, Priority__c, Method__c, Message__c From Custom_Log__c LIMIT 1];
    	System.assertEquals('CustomLogController', customLog.Class__c);
    	System.assertEquals('divideByZeroException', customLog.Method__c);
    	System.assertEquals(CustomLogging.WARNING, customLog.Priority__c);
    }
    
    static testMethod void testNullPointerException(){
    	setEnableCustomLoggingFlag(true);
    	CustomLogController customLogController = new CustomLogController();
    	customLogController.nullPointerException();
    	
    	Integer numberOfCustomLogs= [Select count() From Custom_Log__c];
    	Custom_Log__c customLog= [Select Class__c, Priority__c, Method__c, Message__c From Custom_Log__c LIMIT 1];
    	System.assertEquals('CustomLogController', customLog.Class__c);
    	System.assertEquals('nullPointerException', customLog.Method__c);
    	System.assertEquals(CustomLogging.ERROR, customLog.Priority__c);
    }
}