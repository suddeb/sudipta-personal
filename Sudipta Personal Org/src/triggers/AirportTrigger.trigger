trigger AirportTrigger on Airport__c (after insert) {
	List<Country__c> allCountries = new List<Country__c>();
	List<String> allCountryNames = new List<String>();
	
	//Create a Map
	Map<String, Country__c> allCountryRecords = new Map<String, Country__c>();
	for(Country__c aCountry : [SELECT ID, NAME FROM COUNTRY__C]){
		allCountryRecords.put(aCountry.Name, aCountry);
	}
	
	//Iterate only those Airport records matching the requirement - SOQL# 2
	for(Airport__c anAirport : Trigger.new){
		if((allCountryRecords.get(anAirport.Country__c)) == null){
			if(anAirport.Operational__c){
				Country__c aCountry = new Country__c(
											Name=anAirport.Country__c
											);
				allCountries.add(aCountry);
			}	
		}
	}

	/*
	//Fetch all Country Name and put it into a list - SOQL# 1
	for(Country__c aCountry : [SELECT ID, Name FROM COUNTRY__C]){
		allCountryNames.add(aCountry.Name);
	}
		
	//Iterate only those Airport records matching the requirement - SOQL# 2
	for(Airport__c anAirport : [SELECT ID, Country__c, Operational__c from AIRPORT__C where ID IN :Trigger.new 
									AND Country__c NOT IN :allCountryNames]){
		if((allCountryRecords.get(anAirport.Country__c)) == null){
			if(anAirport.Operational__c){
			Country__c aCountry = new Country__c(
											Name=anAirport.Country__c
											);
			allCountries.add(aCountry);
		}
	}
	*/
	
	//Do a bulk insert
	if(allCountries.size() > 0){
		Database.insert(allCountries);
	}
}