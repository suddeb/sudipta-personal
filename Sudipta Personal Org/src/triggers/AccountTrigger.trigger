trigger AccountTrigger on Account (after delete, after insert, after undelete, 
									after update, before delete, before insert, before update) {
	if(Trigger.isDelete && Trigger.isBefore){
		/* The below approach is not correct as it may hit Governor Limit soon 
		for(Account eachAccount : Trigger.old){
			List<Opportunity> allOpportunities = [SELECT ID FROM OPPORTUNITY WHERE AccountId = :eachAccount.Id];
			if(allOpportunities.size() > 0){
				eachAccount.addError('There are some opportunities associated with this. So can not delete this account');
			}else{
				System.debug('Deleted as no opportunity is associated with this account');
			}
		}
		*/
		
		//Better approach as it is respect Salesforce's Governance Limit
		List<Account> allAccounts = [SELECT ID FROM ACCOUNT WHERE ID IN 
		                             (SELECT ACCOUNTID FROM OPPORTUNITY) 
		                             AND ID IN :Trigger.old];
		for(Account eachAccount : allAccounts){
			Trigger.oldMap.get(eachAccount.Id).addError('There are some opportunities associated with this. So can not delete this account');
		}
	}
}