trigger AccountAddressTrigger on Account (before insert, before update) {
	if(Trigger.isBefore){
		if(Trigger.isInsert || Trigger.isUpdate){
			for(Account eachAccount : Trigger.new){
				if(eachAccount.BillingPostalCode!=null && eachAccount.Match_Billing_Address__c){
					eachAccount.ShippingPostalCode = eachAccount.BillingPostalCode;
				}
			}
		}
	}
}