trigger addRelatedRecord on Account (after insert, after update) {
	List<Opportunity> opportunityList = new List<Opportunity>();
	
	//Get all the accounts which don't have any opportunity associated.
	for(Account eachAccount : [SELECT ID, NAME FROM ACCOUNT WHERE ID IN :Trigger.New 
								AND ID NOT IN (SELECT ACCOUNTID FROM OPPORTUNITY)]){
		opportunityList.add(new Opportunity(Name=eachAccount.Name + ' Opportunity',
											StageName = 'Prospecting',
											CloseDate = System.today().addMonths(1),
											AccountId = eachAccount.Id
											));									
	}
	
	if(opportunityList.size() > 0){
		insert opportunityList;
	}
}