trigger ClosedOpportunityTrigger on Opportunity (after insert, after update) {
	List<Task> allTasks = new List<Task>();
	
	for(Opportunity eachOpportunity :[SELECT ID FROM OPPORTUNITY WHERE ID IN: Trigger.new and StageName = 'Closed Won']){
		allTasks.add(new Task(Subject = 'Follow Up Test Task',
							  WhatId = eachOpportunity.Id));
	}
	
	if(allTasks.size() > 0){
		insert allTasks;
	} 
}